\contentsline {chapter}{Contents}{2}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}About this handout}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Course Summary}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Computer Setup}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}The Ubuntu Desktop}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Workstation setup and program installation}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}Web sites and references}{7}{section.1.6}
\contentsline {chapter}{\chapternumberline {2}Introduction to Circos}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Examples of Circos plots}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Why is Circos so complicated?}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}J-Circos}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Standard genomics data format}{13}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Interactive elements}{15}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Other data types}{16}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Customising figure size and chromosomes}{22}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Circos Table Viewer}{25}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Circos}{26}{chapter.3}
\contentsline {section}{\numberline {3.1}A very brief primer on Linux command-line}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}Circos from command-line}{28}{section.3.2}
\contentsline {section}{\numberline {3.3}A basic Circos plot}{30}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Comments, spaces, blank lines}{30}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Configuration file structure}{30}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Karyotype ideogram}{32}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Chromosomes}{33}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Ideogram block}{35}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Image block}{39}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Ticks block}{39}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Importing files, file paths and overriding values}{40}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Colors, Fonts, Patterns}{41}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}Links}{43}{section.3.5}
\contentsline {section}{\numberline {3.6}Other data types}{46}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Highlights}{46}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Histograms}{47}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Scatterplot}{48}{subsection.3.6.3}
