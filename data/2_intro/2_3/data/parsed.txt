scale atten_large 176 176
scale atten_large 97 97
scale atten_large 83 83
scale atten_large 162 162
scale atten_large 138 138
scale atten_large 151 151
scale atten_large 61 61
scale atten_large 39 39
scale atten_large 54 54
scale atten_large 96 96
scale atten_large 97 97
scale atten_large 110 110
scale atten_large 134 134
scale atten_large 83 83
scale atten_large 131 131
scale atten_large 32 32
hsvpercentile 0 0 1 1
colorpercentile percentile00000 = 255,0,0
hsvpercentile 5 15 1 1
colorpercentile percentile00005 = 255,64,0
hsvpercentile 10 30 1 1
colorpercentile percentile00010 = 255,128,0
hsvpercentile 15 45 1 1
colorpercentile percentile00015 = 255,191,0
hsvpercentile 20 60 1 1
colorpercentile percentile00020 = 255,255,0
hsvpercentile 25 75 1 1
colorpercentile percentile00025 = 191,255,0
hsvpercentile 30 90 1 1
colorpercentile percentile00030 = 128,255,0
hsvpercentile 35 105 1 1
colorpercentile percentile00035 = 64,255,0
hsvpercentile 40 120 1 1
colorpercentile percentile00040 = 0,255,0
hsvpercentile 45 135 1 1
colorpercentile percentile00045 = 0,255,64
hsvpercentile 50 150 1 1
colorpercentile percentile00050 = 0,255,128
hsvpercentile 55 165 1 1
colorpercentile percentile00055 = 0,255,191
hsvpercentile 60 180 1 1
colorpercentile percentile00060 = 0,255,255
hsvpercentile 65 195 1 1
colorpercentile percentile00065 = 0,191,255
hsvpercentile 70 210 1 1
colorpercentile percentile00070 = 0,128,255
hsvpercentile 75 225 1 1
colorpercentile percentile00075 = 0,64,255
hsvpercentile 80 240 1 1
colorpercentile percentile00080 = 0,0,255
hsvpercentile 85 255 1 1
colorpercentile percentile00085 = 64,0,255
hsvpercentile 90 270 1 1
colorpercentile percentile00090 = 128,0,255
hsvpercentile 95 285 1 1
colorpercentile percentile00095 = 191,0,255
hsvpercentile 100 300 1 1
colorpercentile percentile00100 = 255,0,255
table report row 0 A 
table report row 1 B 
table report row 2 C 
table report row 3 D 
table report col 0 A 
table report col 1 B 
table report col 2 C 
table report col 3 D 
table report cell 0 A 0 A raw 176 cleaned/remapped 176* scaled 176 missing? 0 hidden? 0
table report cell 0 A 1 B raw 97 cleaned/remapped 97* scaled 97 missing? 0 hidden? 0
table report cell 0 A 2 C raw 83 cleaned/remapped 83* scaled 83 missing? 0 hidden? 0
table report cell 0 A 3 D raw 162 cleaned/remapped 162* scaled 162 missing? 0 hidden? 0
table report cell 1 B 0 A raw 138 cleaned/remapped 138* scaled 138 missing? 0 hidden? 0
table report cell 1 B 1 B raw 151 cleaned/remapped 151* scaled 151 missing? 0 hidden? 0
table report cell 1 B 2 C raw 61 cleaned/remapped 61* scaled 61 missing? 0 hidden? 0
table report cell 1 B 3 D raw 39 cleaned/remapped 39* scaled 39 missing? 0 hidden? 0
table report cell 2 C 0 A raw 54 cleaned/remapped 54* scaled 54 missing? 0 hidden? 0
table report cell 2 C 1 B raw 96 cleaned/remapped 96* scaled 96 missing? 0 hidden? 0
table report cell 2 C 2 C raw 97 cleaned/remapped 97* scaled 97 missing? 0 hidden? 0
table report cell 2 C 3 D raw 110 cleaned/remapped 110* scaled 110 missing? 0 hidden? 0
table report cell 3 D 0 A raw 134 cleaned/remapped 134* scaled 134 missing? 0 hidden? 0
table report cell 3 D 1 B raw 83 cleaned/remapped 83* scaled 83 missing? 0 hidden? 0
table report cell 3 D 2 C raw 131 cleaned/remapped 131* scaled 131 missing? 0 hidden? 0
table report cell 3 D 3 D raw 32 cleaned/remapped 32* scaled 32 missing? 0 hidden? 0
row/col/label stat row A sum 518 n 4 min 83 max 176 avg 129.5
row/col/label stat row B sum 389 n 4 min 39 max 151 avg 97.25
row/col/label stat row C sum 357 n 4 min 54 max 110 avg 89.25
row/col/label stat row D sum 380 n 4 min 32 max 134 avg 95
row/col/label stat col A sum 502 n 4 min 54 max 176 avg 125.5
row/col/label stat col B sum 427 n 4 min 83 max 151 avg 106.75
row/col/label stat col C sum 372 n 4 min 61 max 131 avg 93
row/col/label stat col D sum 343 n 4 min 32 max 162 avg 85.75
row/col/label stat label A sum 1020 n 8 min 54 max 176 avg 127.5
row/col/label stat label B sum 816 n 8 min 39 max 151 avg 102
row/col/label stat label C sum 729 n 8 min 54 max 131 avg 91.125
row/col/label stat label D sum 723 n 8 min 32 max 162 avg 90.375
labels ordered D C B A
label2seg 0 D D
label2seg 1 C C
label2seg 2 B B
label2seg 3 A A
labels ordered A B C D
colordef colora = 230,46,46
colordef colord = 230,46,230
colordef colorc = 46,168,230
colordef colorb = 107,230,46
highlight row A 0 176 fill_color=colora
highlight row A 176 338 fill_color=colord
highlight row A 338 435 fill_color=colorb
highlight row A 435 518 fill_color=colorc
highlight row C 0 110 fill_color=colord
highlight row C 110 207 fill_color=colorc
highlight row C 207 303 fill_color=colorb
highlight row C 303 357 fill_color=colora
highlight row D 0 134 fill_color=colora
highlight row D 134 265 fill_color=colorc
highlight row D 265 348 fill_color=colorb
highlight row D 348 380 fill_color=colord
highlight row B 0 151 fill_color=colorb
highlight row B 151 289 fill_color=colora
highlight row B 289 350 fill_color=colorc
highlight row B 350 389 fill_color=colord
highlight col A 0 176 fill_color=colora
highlight col A 176 314 fill_color=colorb
highlight col A 314 448 fill_color=colord
highlight col A 448 502 fill_color=colorc
highlight col C 0 131 fill_color=colord
highlight col C 131 228 fill_color=colorc
highlight col C 228 311 fill_color=colora
highlight col C 311 372 fill_color=colorb
highlight col D 0 162 fill_color=colora
highlight col D 162 272 fill_color=colorc
highlight col D 272 311 fill_color=colorb
highlight col D 311 343 fill_color=colord
highlight col B 0 151 fill_color=colorb
highlight col B 151 248 fill_color=colora
highlight col B 248 344 fill_color=colorc
highlight col B 344 427 fill_color=colord
highlight all D 0 296 fill_color=colora
highlight all D 296 537 fill_color=colorc
highlight all D 537 659 fill_color=colorb
highlight all D 659 723 fill_color=colord
highlight all C 0 241 fill_color=colord
highlight all C 241 435 fill_color=colorc
highlight all C 435 592 fill_color=colorb
highlight all C 592 729 fill_color=colora
highlight all B 0 302 fill_color=colorb
highlight all B 302 537 fill_color=colora
highlight all B 537 694 fill_color=colorc
highlight all B 694 816 fill_color=colord
highlight all A 0 352 fill_color=colora
highlight all A 352 648 fill_color=colord
highlight all A 648 883 fill_color=colorb
highlight all A 883 1020 fill_color=colorc
karyotype chr - D D 0 723 colord
segmentlabel D 0 723 D
karyotype chr - C C 0 729 colorc
segmentlabel C 0 729 C
karyotype chr - B B 0 816 colorb
segmentlabel B 0 816 B
karyotype chr - A A 0 1020 colora
segmentlabel A 0 1020 A
chromosomes_scale = D:1.0000,C:1.0000,B:1.0000,A:1.0000
loop D row
loop D col
loop C row
loop C col
loop B row
loop B col
loop A row
loop A col
link cell_0000 A 0 176 color=colora_a1,z=100
link cell_0000 A 518 694 color=colora_a1,z=100
colribboncap A 518 694 fill_color=colora
rowribboncap A 0 176 fill_color=colora
link cell_0001 A 176 273 color=colora_a1,z=45
link cell_0001 B 389 486 color=colora_a1,z=45
colribboncap B 389 486 fill_color=colora
rowribboncap A 176 273 fill_color=colorb
link cell_0002 A 273 356 color=colora_a1,z=35
link cell_0002 C 357 440 color=colora_a1,z=35
colribboncap C 357 440 fill_color=colora
rowribboncap A 273 356 fill_color=colorc
link cell_0003 A 356 518 color=colora_a1,z=90
link cell_0003 D 380 542 color=colora_a1,z=90
colribboncap D 380 542 fill_color=colora
rowribboncap A 356 518 fill_color=colord
link cell_0004 B 0 138 color=colorb_a1,z=74
link cell_0004 A 694 832 color=colorb_a1,z=74
colribboncap A 694 832 fill_color=colorb
rowribboncap B 0 138 fill_color=colora
link cell_0005 B 138 289 color=colorb_a1,z=83
link cell_0005 B 486 637 color=colorb_a1,z=83
colribboncap B 486 637 fill_color=colorb
rowribboncap B 138 289 fill_color=colorb
link cell_0006 B 289 350 color=colorb_a1,z=20
link cell_0006 C 440 501 color=colorb_a1,z=20
colribboncap C 440 501 fill_color=colorb
rowribboncap B 289 350 fill_color=colorc
link cell_0007 B 350 389 color=colorb_a1,z=5
link cell_0007 D 542 581 color=colorb_a1,z=5
colribboncap D 542 581 fill_color=colorb
rowribboncap B 350 389 fill_color=colord
link cell_0008 C 0 54 color=colorc_a1,z=15
link cell_0008 A 832 886 color=colorc_a1,z=15
colribboncap A 832 886 fill_color=colorc
rowribboncap C 0 54 fill_color=colora
link cell_0009 C 54 150 color=colorc_a1,z=44
link cell_0009 B 637 733 color=colorc_a1,z=44
colribboncap B 637 733 fill_color=colorc
rowribboncap C 54 150 fill_color=colorb
link cell_0010 C 150 247 color=colorc_a1,z=45
link cell_0010 C 501 598 color=colorc_a1,z=45
colribboncap C 501 598 fill_color=colorc
rowribboncap C 150 247 fill_color=colorc
link cell_0011 C 247 357 color=colorc_a1,z=54
link cell_0011 D 581 691 color=colorc_a1,z=54
colribboncap D 581 691 fill_color=colorc
rowribboncap C 247 357 fill_color=colord
link cell_0012 D 0 134 color=colord_a1,z=71
link cell_0012 A 886 1020 color=colord_a1,z=71
colribboncap A 886 1020 fill_color=colord
rowribboncap D 0 134 fill_color=colora
link cell_0013 D 134 217 color=colord_a1,z=35
link cell_0013 B 733 816 color=colord_a1,z=35
colribboncap B 733 816 fill_color=colord
rowribboncap D 134 217 fill_color=colorb
link cell_0014 D 217 348 color=colord_a1,z=69
link cell_0014 C 598 729 color=colord_a1,z=69
colribboncap C 598 729 fill_color=colord
rowribboncap D 217 348 fill_color=colorc
link cell_0015 D 348 380 color=colord_a1,z=0
link cell_0015 D 691 723 color=colord_a1,z=0
colribboncap D 691 723 fill_color=colord
rowribboncap D 348 380 fill_color=colord
